import 'package:hive/hive.dart';

part 'polluant.g.dart';

@HiveType(typeId: 1)
class Polluant {
  // ignore: non_constant_identifier_names
  @HiveField(0)
  String? nom;
  @HiveField(1)
  // ignore: non_constant_identifier_names
  String? measure_unity;
  @HiveField(2)
  // ignore: non_constant_identifier_names
  double? seuil_value;
  @HiveField(3)
  // ignore: non_constant_identifier_names
  double? measure_value;

  Polluant(this.nom, this.measure_unity, this.measure_value, this.seuil_value);

  String key() => nom.hashCode.toString();
}
