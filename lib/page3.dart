import 'package:flutter/material.dart';
import 'package:memoire/interface_connexion.dart';
import 'package:memoire/polluant_list.dart';

class Page3 extends StatelessWidget {
  const Page3({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Column(
            children: [
              Container(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: const Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: () => Navigator.of(context).pop(),
                  )),
              const Center(
                child: Text(
                  "Choisissez le lieu de mesure",
                  style: TextStyle(
                      color: Color.fromRGBO(18, 18, 65, 1),
                      fontSize: 30,
                      fontWeight: FontWeight.w800),
                ),
              ),
              const Divider(
                height: 70,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              Expanded(
                child: GridView.count(
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 2,
                  children: <Widget>[
                    Card(
                      color: const Color.fromRGBO(18, 18, 65, 1),
                      child: InkWell(
                        onTap: () {
                          var route = MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  const Polluant_list());
                          Navigator.of(context).push(route);
                        },
                        child: const SizedBox(
                          width: 115,
                          height: 160,
                          child: Center(
                            child: Text(
                              'Urphoran',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: const Color.fromRGBO(18, 18, 65, 1),
                      child: InkWell(
                        onTap: () {
                          var route = MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  const Polluant_list());
                          Navigator.of(context).push(route);
                        },
                        child: const SizedBox(
                          width: 115,
                          height: 160,
                          child: Center(
                            child: Text(
                              'Epac',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: const Color.fromRGBO(18, 18, 65, 1),
                      child: InkWell(
                        onTap: () {
                          debugPrint('Card tapped.');
                        },
                        child: const SizedBox(
                          width: 115,
                          height: 160,
                          child: Center(
                            child: Text(
                              'Fsa',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: const Color.fromRGBO(18, 18, 65, 1),
                      child: InkWell(
                        onTap: () {
                          debugPrint('Card tapped.');
                        },
                        child: const SizedBox(
                          width: 115,
                          height: 160,
                          child: Center(
                            child: Text(
                              'Ifri',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: const Color.fromRGBO(18, 18, 65, 1),
                      child: InkWell(
                        onTap: () {
                          debugPrint('Card tapped.');
                        },
                        child: const SizedBox(
                          width: 115,
                          height: 160,
                          child: Center(
                            child: Text(
                              'Ine',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: const Color.fromRGBO(18, 18, 65, 1),
                      child: InkWell(
                        onTap: () {
                          debugPrint('Card tapped.');
                        },
                        child: const SizedBox(
                          width: 115,
                          height: 160,
                          child: Center(
                            child: Text(
                              'Enam',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 100, vertical: 30),
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: const Color.fromRGBO(18, 18, 250, 1),
                        width: 5.0,
                        style: BorderStyle.solid),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(50),
                    ), //BorderRadius.all
                  ),
                  child: GestureDetector(
                      onTap: () {
                        var route = MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const Interface_connexion());
                        Navigator.of(context).push(route);
                      },
                      child: const Padding(
                        padding: EdgeInsets.only(
                            left: 50, right: 50, top: 7, bottom: 7),
                        child: Text(
                          'Se connecter',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ) //BoxDecoration
                      ))
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
