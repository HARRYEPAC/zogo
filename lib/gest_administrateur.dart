// ignore_for_file: must_be_immutable, camel_case_types

import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:memoire/add_admin.dart';
import 'package:memoire/data.dart';
import 'package:memoire/polluant_add.dart';

import 'databaseBox.dart';

class Gest_administrateur extends StatefulWidget {
  const Gest_administrateur({Key? key}) : super(key: key);
  @override
  State<Gest_administrateur> createState() =>Gest_administrateurState();
}

class Gest_administrateurState extends State<Gest_administrateur> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Scaffold(
        body: ValueListenableBuilder(
            valueListenable: DatabaseBox.box!.listenable(),
            builder: (context, Box items, _) {
              List<String> keys = items.keys.cast<String>().toList();
              return Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Column(
                  children: [
                    Container(
                        alignment: Alignment.topLeft,
                        child: IconButton(
                          icon:
                          const Icon(Icons.arrow_back, color: Colors.black),
                          onPressed: () => Navigator.of(context).pop(),
                        )),
                    const Center(
                      child: Text(
                        "Interface-administrateur",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color.fromRGBO(18, 18, 65, 1),
                            fontSize: 30,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    const Divider(
                      height: 50,
                      thickness: 5,
                      indent: 20,
                      endIndent: 20,
                      color: Colors.black,
                    ),
                    const Center(
                      child: Text(
                        "Liste des administrateurs",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color.fromRGBO(18, 18, 65, 1),
                            fontSize: 25,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Expanded(
                      child: GridView.builder(
                        gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 200,
                            //childAspectRatio: 3 / 2,
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 20),
                        itemCount: keys.length,
                        itemBuilder: (context, index) {
                          final recipe = items.get(keys[index]);
                          return Dismissible(
                              key: Key(recipe.username),
                              onDismissed: (direction) {
                                setState(() {
                                  DatabaseBox.box!.delete(recipe.key());
                                });
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                        Text("${recipe.username} supprimé")));
                              },
                              background: Container(color: Colors.red),
                              child: Container( alignment: Alignment.center,
                                  child: RecipeItemWidget(polluant: recipe)));
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 300),
                      child: FloatingActionButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Add_admin()));
                          //Navigator.pop(context);
                        },
                        child: const Icon(Icons.add),
                      ),
                    ),
                  ],
                ),
              );
            }),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class RecipeItemWidget extends StatefulWidget {
  const RecipeItemWidget({Key? key, required this.polluant}) : super(key: key);
  final Data polluant;

  @override
  State<RecipeItemWidget> createState() => _RecipeItemWidgetState();
}

class _RecipeItemWidgetState extends State<RecipeItemWidget> {
  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //var val = widget.polluant.seuil_value;
    return Card(
      color: const Color.fromRGBO(30, 170, 255, 0.5),
      child: InkWell(
        onTap: () {

        },
        child: SizedBox(
            width: 115,
            height: 160,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.polluant.username.toString(),
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
              ],
            )),
      ),
    );
  }
}
