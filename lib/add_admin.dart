import 'package:memoire/databaseBox.dart';
import 'package:flutter/material.dart';
import 'data.dart';
class Add_admin extends StatefulWidget {
  const Add_admin({Key? key}) : super(key: key);
  @override
  State<Add_admin> createState() => Add_adminState();
}

class Add_adminState extends State<Add_admin> {
  final formKey = GlobalKey<
      FormState>(); // Permettre de valider les champs de session pour voir si ils sont vide ou pas
  final nomController = TextEditingController();
  // ignore: non_constant_identifier_names
  final measure_unityController = TextEditingController();
  /* final seuil_valueController= TextEditingValue();
  final measured_valueController= TextEditingValue();*/

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    nomController.dispose();
    measure_unityController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 40),
        child: Column(children: [
          Container(
              alignment: Alignment.topLeft,
              child: IconButton(
                icon: const Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () => Navigator.of(context).pop(),
              )),
          const Text(
            "Ajouter un administrateur",
            style: TextStyle(
                color: Color.fromRGBO(18, 18, 65, 1),
                fontSize: 30,
                fontWeight: FontWeight.w800),
            textAlign: TextAlign.center,
          ),
          const Divider(
            height: 30,
            thickness: 5,
            indent: 100,
            endIndent: 100,
            color: Colors.black,
          ),
          Form(
              key: formKey,
              child: Expanded(
                child: ListView(children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: nomController,
                      decoration: const InputDecoration(
                          labelText: 'Nom d\'utilisateur',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(20)))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Entrer un nom d\'utilisateur';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: TextFormField(
                      controller: measure_unityController,
                      decoration: const InputDecoration(
                          labelText: 'Mot de Passe',
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(20)))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Entrer un mot de passe';
                        }
                        return null;
                      },
                    ),
                  ),
                  /*Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller:  seuil_valueController,
                        decoration: const InputDecoration(
                            labelText: 'Valeur seuil',
                            border:OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20))
                            )
                        ),
                        validator: (value){
                          if(value!.isEmpty){
                            return 'Please enter a title';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller:  measured_valueController,
                        decoration: const InputDecoration(
                            labelText: 'Valeur mesurée',
                            border:OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20))
                            )
                        ),
                        validator: (value){
                          if(value!.isEmpty){
                            return 'Please enter a measure value';
                          }
                          return null;
                        },
                      ),
                    ),*/
                  const SizedBox(
                    height: 8,
                  ),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          Data  recipe = Data(
                              nomController.value.text,
                              measure_unityController.value.text,
                             );
                          DatabaseBox.box!.put(recipe.key(), recipe);
                          Navigator.pop(context);
                        }
                      },
                      child: const Text(
                        'Enregistrer',
                        style: TextStyle(color: Colors.black, fontSize: 20),
                      ),
                    ),
                  )
                ]),
              )),
        ]),
      ),
    );
  }
}
