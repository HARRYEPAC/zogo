import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:memoire/databaseBox.dart';
import 'package:memoire/polluant.dart';
import 'package:memoire/polluants_mesure.dart';
import 'interface_connexion.dart';

class Polluant_list extends StatefulWidget {
  const Polluant_list({super.key});

  @override
  State<StatefulWidget> createState() {
    return Polluant_listState();
  }
}

class Polluant_listState extends State<Polluant_list> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ValueListenableBuilder(
            valueListenable: DatabaseBox.box1!.listenable(),
            builder: (context, Box items, _) {
              List<String> keys = items.keys.cast<String>().toList();
              return Padding(
                padding: const EdgeInsets.only(top: 25),
                child: Column(children: [
                  Container(
                      alignment: Alignment.topLeft,
                      child: IconButton(
                        icon: const Icon(Icons.arrow_back, color: Colors.black),
                        onPressed: () => Navigator.of(context).pop(),
                      )),
                  const Text(
                    "Paramètres environnementaux",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color.fromRGBO(18, 18, 65, 1),
                        fontSize: 30,
                        fontWeight: FontWeight.w800),
                  ),
                  const Divider(
                    height: 30,
                    thickness: 5,
                    indent: 20,
                    endIndent: 20,
                    color: Colors.black,
                  ),
                  Expanded(
                    child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 200,
                              //childAspectRatio: 3 / 2,
                              crossAxisSpacing: 5,
                              mainAxisSpacing: 20),
                      itemCount: keys.length,
                      itemBuilder: (context, index) {
                        final recipe = items.get(keys[index]);
                        return Center(
                            child: RecipeItemWidget(polluant: recipe));
                      },
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 100, vertical: 30),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color.fromRGBO(18, 18, 250, 1),
                            width: 5.0,
                            style: BorderStyle.solid),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(50),
                        ), //BorderRadius.all
                      ),
                      child: GestureDetector(
                          onTap: () {
                            var route = MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const Interface_connexion());
                            Navigator.of(context).push(route);
                          },
                          child: const Padding(
                            padding: EdgeInsets.only(
                                left: 50, right: 50, top: 7, bottom: 7),
                            child: Text(
                              'Se connecter',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ) //BoxDecoration
                          )),
                ]),
              );
            }),
      ),
    );
  }
}

class RecipeItemWidget extends StatelessWidget {
  const RecipeItemWidget({Key? key, required this.polluant}) : super(key: key);
  final Polluant polluant;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: const Color.fromRGBO(18, 18, 65, 1),
      child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Polluants_mesure(
                          polluant: polluant,
                        )));
          },
          child: SizedBox(
            width: 160,
            height: 160,
            child: Center(
              child: Text(
                polluant.nom.toString(),
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),

              // ),
              // ),
            ),
          )),
    );
  }
}
