import 'package:flutter/material.dart';
import 'package:memoire/apropos.dart';
import 'package:memoire/page3.dart';

class Page2 extends StatelessWidget {
  const Page2({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 1),
          child: Center(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: const Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: () => Navigator.of(context).pop(),
                  )),
              const Text(
                "Bienvenue sur Qualité de l'air",
                style: TextStyle(
                    color: Color.fromRGBO(18, 18, 65, 1),
                    fontWeight: FontWeight.w700,
                    fontSize: 25),
              ),
              Image.asset(
                'images/aqi1.png',
                height: 225,
                width: 275,
                //fit: BoxFit.cover,
                fit: BoxFit.fill,
              ),
              Container(
                margin: const EdgeInsets.all(30),
                child: const Text(
                  "L'application qui vous permet d'apprécier la qualité d'air sur la base de polluants",
                  style: TextStyle(
                      color: Color.fromRGBO(18, 18, 60, 1),
                      fontWeight: FontWeight.w500,
                      fontSize: 15),
                ),
              ),
              GestureDetector(
                  onTap: () {
                    var route = MaterialPageRoute(
                        builder: (BuildContext context) => const Apropos());
                    Navigator.of(context).push(route);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        'A propos',
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontWeight: FontWeight.w500,
                            fontSize: 15),
                      ), //BoxDecoration
                    ],
                  )),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 100),
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: const Color.fromRGBO(18, 18, 250, 1),
                        width: 4.0,
                        style: BorderStyle.solid),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(50),
                    ), //BorderRadius.all
                  ),
                  child: GestureDetector(
                      onTap: () {
                        var route = MaterialPageRoute(
                            builder: (BuildContext context) => const Page3());
                        Navigator.of(context).push(route);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text('Suivant'), //BoxDecoration
                          Icon(Icons.arrow_right_alt_rounded),
                        ],
                      ))),
            ],
          )),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
