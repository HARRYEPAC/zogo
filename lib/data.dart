import 'package:hive/hive.dart';

part 'data.g.dart';

@HiveType(typeId: 0)
class Data {
  @HiveField(0)
  String? username;
  @HiveField(1)
  String? password;
  Data(this.username, this.password);

  String key() => username.hashCode.toString();
  // ignore: non_constant_identifier_names
}
