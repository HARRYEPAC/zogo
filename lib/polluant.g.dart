// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'polluant.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PolluantAdapter extends TypeAdapter<Polluant> {
  @override
  final int typeId = 1;

  @override
  Polluant read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Polluant(
      fields[0] as String?,
      fields[1] as String?,
      fields[3] as double?,
      fields[2] as double?,
    );
  }

  @override
  void write(BinaryWriter writer, Polluant obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.nom)
      ..writeByte(1)
      ..write(obj.measure_unity)
      ..writeByte(2)
      ..write(obj.seuil_value)
      ..writeByte(3)
      ..write(obj.measure_value);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PolluantAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
