import 'dart:math';
import 'package:memoire/databaseBox.dart';
import 'package:flutter/material.dart';
import 'package:memoire/polluant.dart';

class Polluant_add extends StatefulWidget {
  const Polluant_add({Key? key}) : super(key: key);
  @override
  State<Polluant_add> createState() => Polluant_addState();
}

class Polluant_addState extends State<Polluant_add> {
  final formKey = GlobalKey<
      FormState>(); // Permettre de valider les champs de session pour voir si ils sont vide ou pas
  final nomController = TextEditingController();
  // ignore: non_constant_identifier_names
  final measure_unityController = TextEditingController();
  /* final seuil_valueController= TextEditingValue();
  final measured_valueController= TextEditingValue();*/

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    nomController.dispose();
    measure_unityController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 40),
        child: Column(children: [
          Container(
              alignment: Alignment.topLeft,
              child: IconButton(
                icon: const Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () => Navigator.of(context).pop(),
              )),
          const Text(
            "Paramètres environnementaux",
            style: TextStyle(
                color: Color.fromRGBO(18, 18, 65, 1),
                fontSize: 30,
                fontWeight: FontWeight.w800),
            textAlign: TextAlign.center,
          ),
          const Divider(
            height: 30,
            thickness: 5,
            indent: 100,
            endIndent: 100,
            color: Colors.black,
          ),
          Form(
              key: formKey,
              child: Expanded(
                child: ListView(children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: nomController,
                      decoration: const InputDecoration(
                          labelText: 'Paramètre',
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter a nom';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: TextFormField(
                      controller: measure_unityController,
                      decoration: const InputDecoration(
                          labelText: 'Unité de mesure',
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)))),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter a measure unity';
                        }
                        return null;
                      },
                    ),
                  ),
                  /*Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller:  seuil_valueController,
                        decoration: const InputDecoration(
                            labelText: 'Valeur seuil',
                            border:OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20))
                            )
                        ),
                        validator: (value){
                          if(value!.isEmpty){
                            return 'Please enter a title';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller:  measured_valueController,
                        decoration: const InputDecoration(
                            labelText: 'Valeur mesurée',
                            border:OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20))
                            )
                        ),
                        validator: (value){
                          if(value!.isEmpty){
                            return 'Please enter a measure value';
                          }
                          return null;
                        },
                      ),
                    ),*/
                  const SizedBox(
                    height: 8,
                  ),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          Polluant recipe = Polluant(
                              nomController.value.text,
                              measure_unityController.value.text,
                              Random().nextDouble(),
                              Random().nextDouble());
                          DatabaseBox.box1!.put(recipe.key(), recipe);
                          Navigator.pop(context);
                        }
                      },
                      child: const Text(
                        'Enregistrer',
                        style: TextStyle(color: Colors.black, fontSize: 20),
                      ),
                    ),
                  )
                ]),
              )),
        ]),
      ),
    );
  }
}
