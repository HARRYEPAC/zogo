import 'package:flutter/material.dart';

class Apropos extends StatelessWidget {
  const Apropos({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Column(
            children: [
              Container(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: const Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: () => Navigator.of(context).pop(),
                  )),
              const Center(
                child: Text(
                  "Pollution atmosphérique",
                  style: TextStyle(
                      color: Color.fromRGBO(18, 18, 65, 1),
                      fontSize: 30,
                      fontWeight: FontWeight.w800),
                ),
              ),
              const Divider(
                height: 70,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              Container(
                  padding: const EdgeInsets.symmetric(vertical: 100),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(20),
                        child: Center(
                          child: Text(
                            "A propos",
                            style: TextStyle(
                                color: Colors
                                    .black38 /*Color.fromRGBO(18, 18, 65, 1)*/,
                                fontSize: 30,
                                fontWeight: FontWeight.w800),
                          ),
                        ),
                      ),
                      Card(
                        color: Colors.white,
                        child: InkWell(
                          onTap: () {},
                          child: const SizedBox(
                            width: 350,
                            //height: 150,
                            child: Padding(
                              padding: EdgeInsets.all(20),
                              child: Text(
                                "L'application a été développée dans l'intension de rendre la surveillance de la qualité d'aire accessible à tous. L'approche de cette application se base sur la mesure de paramètres environnementaux tels que le taux de monoxyde de carbone (CO), le dioxyde de Carbone(CO2), la température et la pression  ",
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                    color: Colors
                                        .black /*Color.fromRGBO(18, 18, 65, 1)*/,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ))
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
