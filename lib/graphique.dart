import 'package:flutter/material.dart';

import 'interface_connexion.dart';

class Graphique extends StatelessWidget {
  const Graphique({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Column(
            children: [
              Container(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: const Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: () => Navigator.of(context).pop(),
                  )),
              const Center(
                child: Text(
                  "Urphoran-polluants-graphique",
                  style: TextStyle(
                      color: Color.fromRGBO(18, 18, 65, 1),
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const Divider(
                height: 70,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              Expanded(
                child: Image.asset(
                  'images/aqi1.png',
                  height: 300,
                  width: 350,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 100, vertical: 30),
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: const Color.fromRGBO(18, 18, 250, 1),
                        width: 5.0,
                        style: BorderStyle.solid),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(50),
                    ), //BorderRadius.all
                  ),
                  child: GestureDetector(
                      onTap: () {
                        var route = MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const Interface_connexion());
                        Navigator.of(context).push(route);
                      },
                      child: const Padding(
                        padding: EdgeInsets.only(
                            left: 50, right: 50, top: 7, bottom: 7),
                        child: Text(
                          'Se connecter',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ) //BoxDecoration
                      ))
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
