// ignore_for_file: file_names

import 'package:hive/hive.dart';
import 'package:memoire/polluant.dart';
import 'package:path_provider/path_provider.dart';
import 'data.dart';

class DatabaseBox {
  static final DatabaseBox instance = DatabaseBox();
  static Box? box, box1;

  static init() async {
    final dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    Hive.registerAdapter(DataAdapter());
    Hive.registerAdapter(PolluantAdapter());
    box = await Hive.openBox("data");
    box1 = await Hive.openBox("polluants");
    var values = box!.values.toList();
    var values1 = box1!.values.toList();
    if (values == null || values.isEmpty) {
      DatabaseBox.box?.putAll({for (var e in data) e.key(): e});
    }
    if (values1 == null || values1.isEmpty) {
      DatabaseBox.box1?.putAll({for (var e in polluant) e.key(): e});
    }
  }

  static final List<Data> data = [
    Data(
      "Rodsone",
      "Rodsone1",
    ),
    Data(
      "Michel",
      "Michel1",
    ),
  ];
  static final List<Polluant> polluant = [
    Polluant("Monoxyde de Carbone (CO)", "ppm", 50, 50),
    Polluant("Dioxyde de Carbone (CO2)", "ppm", 60, 70),
    Polluant("Dioxyde d'Azote (NO2)", "ppm", 60, 70),
    Polluant("Dioxyde de souffre (SO2)", "ppm", 60, 70),
    Polluant("Matière particulaire (PM2.5)", "mg/cm3", 60, 70),
    Polluant("Température", "degré Celcius", 60, 70),
    Polluant("Pression", "Pascal Pa", 60, 70),
  ];
}
