import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:memoire/data.dart';
import 'package:memoire/interface_admin.dart';
import 'apropos.dart';
import 'databaseBox.dart';

class Interface_connexion extends StatefulWidget {
  const Interface_connexion({Key? key}) : super(key: key);
  @override
  State<Interface_connexion> createState() => Interface_connexionState();
}

class Interface_connexionState extends State<Interface_connexion> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: ValueListenableBuilder(
            valueListenable: DatabaseBox.box!.listenable(),
            builder: (context, Box items, _) {
              List<String> keys = items.keys.cast<String>().toList();
              final recipe1 = items.get(keys[0]);
              final recipe = items.get(keys[1]);
              return Interface_connexion1(
                polluant: recipe1,
                polluant1: recipe,
              );
              /* itemBuilder: (context, index) {
                    final recipe = items.get(keys[index]);
                    return Center(
                        key: Key(recipe.username),
                        child:Interface_connexion());
                    });*/
            }),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class Interface_connexion1 extends StatefulWidget {
  const Interface_connexion1(
      {Key? key, required this.polluant, required this.polluant1})
      : super(key: key);
  final Data polluant;
  final Data polluant1;

  @override
  State<Interface_connexion1> createState() => _Interface_connexion1State();
}

class _Interface_connexion1State extends State<Interface_connexion1> {
  final formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController1 = TextEditingController();

  //Data data = Data();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    usernameController.dispose();
    passwordController1.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            scaffoldBackgroundColor: const Color.fromRGBO(18, 18, 250, 1)),
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Center(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: Column(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                alignment: Alignment.topLeft,
                                child: IconButton(
                                  icon: const Icon(Icons.arrow_back,
                                      color: Colors.black),
                                  onPressed: () => Navigator.of(context).pop(),
                                )),
                            ClipOval(
                                child: Image.asset(
                              'images/aqi1.png',
                              height: 125,
                              width: 125,
                              fit: BoxFit.fill,
                            )),
                            GestureDetector(
                              onTap: () {
                                debugPrint(widget.polluant.username);
                                debugPrint(widget.polluant.password);
                              },
                              child: Container(
                                padding: const EdgeInsets.only(top: 10),
                                child: const Text(
                                  "Qualité de l'air - plateforme",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25),
                                ),
                              ),
                            ),
                            const Divider(
                              height: 40,
                              thickness: 5,
                              indent: 125,
                              endIndent: 125,
                              color: Colors.white,
                            ),
                            const Text(
                              "CONNEXION",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 25),
                            ),
                            const Divider(
                              height: 15,
                              thickness: 2,
                              indent: 125,
                              endIndent: 125,
                              color: Color.fromRGBO(18, 18, 250, 1),
                            ),
                          ],
                        ))),
                Expanded(
                  child: Center(
                    child: Form(
                      key: formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            controller: usernameController,
                            decoration: const InputDecoration(
                                labelText: 'Nom d\'utilisateur',
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20)))),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter a nom';
                              }
                              return null;
                            },
                          ),
                          Container(
                            height: 20,
                          ),
                          TextFormField(
                            controller: passwordController1,
                            decoration: const InputDecoration(
                                labelText: 'Mot de Passe',
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20)))),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Veuillez entrez un mot de passe';
                              }
                              return null;
                            },
                          ),
                          Container(
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 30),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color:
                                        const Color.fromRGBO(255, 255, 255, 1),
                                    width: 5.0,
                                    style: BorderStyle.solid),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(50),
                                ), //BorderRadius.all
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    String red =
                                        usernameController.text;
                                    String red1 =
                                        widget.polluant.username.toString();
                                    String red2 =
                                        passwordController1.text;
                                    String red3 =
                                        widget.polluant.password.toString();
                                   String red4 =
                                        widget.polluant1.username.toString();
                                    String red5 =
                                        widget.polluant1.password.toString();

                                    // &&(red==red1)
                                    if ((formKey.currentState!.validate()) &&
                                        (((red == red1) && (red2 == red3)) ||
                                            ((red == red4) &&
                                                (red2 == red5)))) {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const Interface_admin()));
                                      //Navigator.pop(context);
                                    } else {
                                      const snackBar = SnackBar(
                                        content: Text(
                                            'Nom d\'utilisateur ou mot de passe erroné'),
                                      );

                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    }
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.only(
                                        left: 50, right: 50, top: 7, bottom: 7),
                                    child: Text(
                                      'Se connecter à l\'interface admin',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ) //BoxDecoration
                                  )),
                          GestureDetector(
                            onTap: () {
                              debugPrint('click on edit');
                            },
                            child: const Text(
                              "Mot de passe oublié ?",
                              style: TextStyle(color: Colors.white),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    var route = MaterialPageRoute(
                        builder: (BuildContext context) => const Apropos());
                    Navigator.of(context).push(route);
                  },
                  child: const Text(
                    "A propos de la plateforme",
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
