import 'package:flutter/material.dart';
import 'package:memoire/graphique.dart';
import 'package:memoire/polluant.dart';
import 'interface_connexion.dart';

String? nom;

// ignore: camel_case_types
class Polluants_mesure extends StatelessWidget {
  const Polluants_mesure({Key? key, required this.polluant}) : super(key: key);
  final Polluant polluant;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: myhome1(polluant: polluant),
    );
  }
}

/*class MyHomePage extends StatefulWidget {
  @override
  // ignore: library_private_types_in_public_api
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false,
        home:myhome1()
        //his trailing comma makes auto-formatting nicer for build methods.
        );
  }
}*/
class myhome1 extends StatefulWidget {
  const myhome1({Key? key, required this.polluant}) : super(key: key);
  final Polluant polluant;
  @override
  State<myhome1> createState() => _myhome1State();
}

class _myhome1State extends State<myhome1> {
  @override
  Widget build(BuildContext context) {
    var val = widget.polluant.seuil_value;
    var val1 = widget.polluant.measure_value;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                alignment: Alignment.topLeft,
                child: GestureDetector(
                  onTap: ()=>{
                    Navigator.of(context).pop(),
                  },
                  child: const Icon(Icons.arrow_back, color: Colors.black),
                )),
            Center(
              child: Text(
                widget.polluant.nom.toString(),
                style: const TextStyle(
                    color: Color.fromRGBO(18, 18, 65, 1),
                    fontSize: 30,
                    fontWeight: FontWeight.w800),
                textAlign: TextAlign.center,
              ),
            ),
            const Divider(
              height: 30,
              thickness: 5,
              indent: 100,
              endIndent: 100,
              color: Colors.black,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  //const Padding(padding: EdgeInsets.symmetric(vertical: 10)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //const Padding(padding: EdgeInsets.only(left: 0)),
                      const Text(
                        ' Unité de mesure',
                        style: TextStyle(
                          color: Color.fromRGBO(18, 18, 65, 1),
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4)),
                      Container(
                        //margin: const EdgeInsets.all(100.0),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 2.5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 1,
                                color: const Color.fromRGBO(0, 0, 0, 1))),
                        child: Text(widget.polluant.measure_unity.toString()),
                      ),
                    ],
                  ),
                  //const Padding(padding: EdgeInsets.symmetric(vertical: 15)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //const Padding(padding: EdgeInsets.only(left: 5)),
                      const Text(
                        'Valeur seuil',
                        style: TextStyle(
                          color: Color.fromRGBO(18, 18, 65, 1),
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20)),
                      Container(
                        //margin: const EdgeInsets.all(100.0),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 2.5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 1,
                                color: const Color.fromRGBO(0, 0, 0, 1))),
                        child: Text('$val'),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //const Padding(padding: EdgeInsets.only(left: 5)),
                      const Text(
                        'Valeur mesurée ',
                        style: TextStyle(
                          color: Color.fromRGBO(18, 18, 65, 1),
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 1)),
                      Container(
                        //margin: const EdgeInsets.all(100.0),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 55, vertical: 2.5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 1,
                                color: const Color.fromRGBO(0, 0, 0, 1))),
                        child: Text('$val1'),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(right: 45, left: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        //const Padding(padding: EdgeInsets.only(left: 5)),
                        const Text(
                          'Graphique',
                          style: TextStyle(
                            color: Color.fromRGBO(18, 18, 65, 1),
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 65)),
                        IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const Graphique()));
                            },
                            icon: const Icon(
                              Icons.bar_chart,
                              color: Colors.black,
                              size: 35,
                            ))
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //const Padding(padding: EdgeInsets.only(left: 5)),
                      const Text(
                        'Appréciation',
                        style: TextStyle(
                          color: Color.fromRGBO(18, 18, 65, 1),
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15)),
                      Container(
                        //margin: const EdgeInsets.all(100.0),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 5,
                                color: const Color.fromRGBO(0, 0, 0, 1))),
                        child: (val1! < val!)
                            ? const Text(
                                "Bon",
                                style: (TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15)),
                              )
                            : const Text(
                                "Mauvais",
                                style: (TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15)),
                              ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 100, vertical: 30),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: const Color.fromRGBO(18, 18, 250, 1),
                      width: 5.0,
                      style: BorderStyle.solid),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(50),
                  ), //BorderRadius.all
                ),
                child: GestureDetector(
                    onTap: () {
                      var route = MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const Interface_connexion());
                      Navigator.of(context).push(route);
                    },
                    child: const Padding(
                      padding: EdgeInsets.only(
                          left: 50, right: 50, top: 7, bottom: 7),
                      child: Text(
                        'Se connecter',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ) //BoxDecoration
                    ))
          ],
        ),
      ),
    );
  }
}
