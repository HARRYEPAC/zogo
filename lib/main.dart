import 'package:flutter/material.dart';
import 'package:memoire/databaseBox.dart';
import 'package:memoire/page2.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DatabaseBox.init();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          scaffoldBackgroundColor: const Color.fromRGBO(18, 18, 250, 1)),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home:
          const Main1(), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

// ignore: must_be_immutable
class Main1 extends StatelessWidget {
  const Main1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ClipOval(
              child: Image.asset(
            'images/aqi1.png',
            height: 150,
            width: 150,
            fit: BoxFit.fill,
          )),
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const Page2()));
            },
            child: Container(
              padding: const EdgeInsets.only(top: 20),
              child: const Text(
                "Qualité de l'air",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25),
              ),
            ),
          ),
        ],
      )),
    );
  }
}
