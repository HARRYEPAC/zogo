// ignore_for_file: must_be_immutable, camel_case_types

import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:memoire/gest_administrateur.dart';
import 'package:memoire/page2.dart';
import 'package:memoire/polluant.dart';
import 'package:memoire/polluant_add.dart';

import 'databaseBox.dart';

class Interface_admin extends StatefulWidget {
  const Interface_admin({Key? key}) : super(key: key);
  @override
  State<Interface_admin> createState() => _Interface_adminState();
}

class _Interface_adminState extends State<Interface_admin> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Scaffold(
        body: ValueListenableBuilder(
            valueListenable: DatabaseBox.box1!.listenable(),
            builder: (context, Box items, _) {
              List<String> keys = items.keys.cast<String>().toList();
              return Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Column(
                  children: [
                    Container(
                        alignment: Alignment.topLeft,
                        child: IconButton(
                          icon:
                              const Icon(Icons.arrow_back, color: Colors.black),
                          onPressed: () => Navigator.of(context).pop(),
                        )),
                    const Center(
                      child: Text(
                        "Interface-administrateur",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color.fromRGBO(18, 18, 65, 1),
                            fontSize: 30,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    const Divider(
                      height: 50,
                      thickness: 5,
                      indent: 20,
                      endIndent: 20,
                      color: Colors.black,
                    ),
                    const Center(
                      child: Text(
                        "Liste des polluants disponibles",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color.fromRGBO(18, 18, 65, 1),
                            fontSize: 25,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Expanded(
                      child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 200,
                                //childAspectRatio: 3 / 2,
                                crossAxisSpacing: 5,
                                mainAxisSpacing: 20),
                        itemCount: keys.length,
                        itemBuilder: (context, index) {
                          final recipe = items.get(keys[index]);
                          return Dismissible(
                              key: Key(recipe.nom),
                              onDismissed: (direction) {
                                setState(() {
                                  DatabaseBox.box1!.delete(recipe.key());
                                });
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text("${recipe.nom} supprimé")));
                              },
                              background: Container(color: Colors.red),
                              child: Center(
                                  child: RecipeItemWidget(polluant: recipe)));
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 300),
                      child: FloatingActionButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Polluant_add()));
                          //Navigator.pop(context);
                        },
                        child: const Icon(Icons.add),
                      ),
                    ),
                  ],
                ),
              );
            }),
        persistentFooterButtons: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 2),
            decoration: BoxDecoration(
              border: Border.all(
                  color: const Color.fromRGBO(18, 18, 250, 1),
                  width: 5.0,
                  style: BorderStyle.solid),
              borderRadius: const BorderRadius.all(
                Radius.circular(50),
              ), //BorderRadius.all
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Page2()));
                      },
                      icon: const Icon(
                        Icons.home,
                        color: Colors.blue,
                      )),
                  const Text("Acceuil", style: TextStyle(color: Colors.black))
                ]),
                const Padding(padding: EdgeInsets.symmetric(horizontal: 75)),
                Column(children: [
                  IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Gest_administrateur()));
                      },
                      icon: const Icon(
                        Icons.person,
                        color: Colors.blue,
                      )),
                  const Text("Adminitrateurs", style: TextStyle(color: Colors.black))
                ]),
              ],
            ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class RecipeItemWidget extends StatefulWidget {
  const RecipeItemWidget({Key? key, required this.polluant}) : super(key: key);
  final Polluant polluant;

  @override
  State<RecipeItemWidget> createState() => _RecipeItemWidgetState();
}

class _RecipeItemWidgetState extends State<RecipeItemWidget> {
  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var val = widget.polluant.seuil_value;
    return Card(
      color: const Color.fromRGBO(30, 170, 255, 0.5),
      child: InkWell(
        onTap: () {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: Center(child: Text(widget.polluant.nom.toString())),
              content: TextField(
                controller: myController,
                obscureText: false,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Valeur seuil',
                ),
              ), //const Text('AlertDialog description'),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    /*var route = MaterialPageRoute(
                        builder: (BuildContext context) =>  Polluants_mesure());
                    Navigator.of(context).push(route);*/
                  },
                  child: const Text('Modifier'),
                ),
              ],
            ),
          );
        },
        child: SizedBox(
            width: 115,
            height: 160,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.polluant.nom.toString(),
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
                const Divider(
                  height: 25,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                  color: Color.fromRGBO(30, 170, 255, 0.5),
                ),
                Text(
                  'Valeur seuil: $val',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
              ],
            )),
      ),
    );
  }
}
